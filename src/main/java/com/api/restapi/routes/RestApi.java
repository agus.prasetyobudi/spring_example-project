package com.api.restapi.routes;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.PutMapping;
// import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
// import org.springframework.web.bind.annotation.PathVariable;

import com.api.restapi.models.RestModels;
import com.api.restapi.services.RestServices;
// import org.springframework.web.bind.annotation.RequestParam;


@RestController
@RequestMapping("/api/user")
/**
 * RestApi
 */
public class RestApi {
    private RestServices theServices;

    @Autowired
    public RestApi(RestServices services){
        theServices = services;
    }

    @GetMapping("/getall")
    public List<RestModels> findAll(){
        return theServices.findAll();
    }
}