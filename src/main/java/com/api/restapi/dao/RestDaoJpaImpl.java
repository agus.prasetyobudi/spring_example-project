package com.api.restapi.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Repository;

import com.api.restapi.models.RestModels;

/**
 * RestDaoJpaImpl
 */
@Repository
public class RestDaoJpaImpl implements RestDao {  
	
    private EntityManager managers;

    @Autowired
    public RestDaoJpaImpl(EntityManager theManager){
        managers = theManager;
    }
 
	@Override
    public List<RestModels> findAll(){
        Query theQueryGet = managers.createQuery("from RestModels");
        List<RestModels> theQueryRes = theQueryGet.getResultList();
        
        return theQueryRes;
    }

    @Override
    public RestModels findById(int Ids){
        RestModels resQuery = managers.find(RestModels.class, Ids);
        return resQuery;
    }

    @Override
    public void save(RestModels params){
        RestModels theQuery = managers.merge(params); 

        params.setId(theQuery.getId());
    }

    @Override
    public void deleted(int Ids){
        Query getQuery = managers.createQuery("delete from users where id=:userid");
        getQuery.setParameter("userid", Ids);

        getQuery.executeUpdate();
    }
    
}