package com.api.restapi.dao;

import java.util.List;

//import org.springframework.stereotype.Repository;

import com.api.restapi.models.RestModels;

//@Repository
public interface RestDao {
    public List<RestModels> findAll();

    public RestModels findById(int Ids);

    public void save(RestModels params);

    public void deleted(int Ids);
}