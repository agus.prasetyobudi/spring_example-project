package com.api.restapi.dao;

import java.util.List;

import com.api.restapi.models.RestModels;

public interface MyDao {
	
	public List<RestModels> findAll();

    public RestModels findById(int Ids);

    public void save(RestModels params);

    public void deleted(int Ids);

}
