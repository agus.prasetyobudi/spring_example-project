package com.api.restapi.services;

import java.util.List;
import com.api.restapi.models.RestModels;
/**
 * RestServices
 */
public interface RestServices {

    public List<RestModels> findAll();

    public RestModels findById(int Ids);

    public void save(RestModels params);

    public void deleted(int Ids);
}