package com.api.restapi.services;

import java.util.List; 
 
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.api.restapi.dao.RestDao;
import com.api.restapi.models.RestModels;
/**
 * RestServicesImpl
 */

 /**
  * RestServicesImpl
  */
  @Service
 public class RestServicesImpl implements RestServices {
	 @Autowired
     private RestDao daoManager;

     @Autowired
     public RestServicesImpl(@Qualifier("restDaoJpaImpl") RestDao restDao){
        daoManager = restDao;
     }

     @Override
     @Transactional
     public List<RestModels> findAll(){
         return daoManager.findAll();
     }

     @Override
     @Transactional
     public RestModels findById(int Ids){
         return daoManager.findById(Ids);
     }

     @Override
     @Transactional
     public void save(RestModels params){
         daoManager.save(params);
     }

     @Override
     @Transactional
     public void deleted(int Ids){
         daoManager.deleted(Ids);
     }
 }